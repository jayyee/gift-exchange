The source code for this project is under the AGPLv3 license, except
for [canvasSnow.min.js](https://github.com/bjldigital/snowfallcanvas),
which is included in the demo and under the MIT license.