import { Map, Set, List, OrderedMap, fromJS } from 'immutable';
import { membersData } from './data';
import legacyDataJSON from './exchangeHistory.json';

const legacyData = fromJS(legacyDataJSON).toOrderedMap();

function giftFam(shuffledList, year) {
  return shuffledList.map((member, index) => Map({
    source: member,
    target: shuffledList.get(index - 1),
    value: year,
  }));
}

function validatePairings(exchanges, previousData) {
  const incestThreshold = 2;
  const uniqueYears = 3;
  const repeatsAllowed = 0;
  function diverse() {
    return exchanges.map(exchange => Set.intersect([
      exchange.get('source').get('teams').toSet(),
      exchange.get('target').get('teams').toSet(),
    ])).filter(intersects => intersects.size > 0)
                    .size <= incestThreshold;
  }

  function notRedundant() {
    const exchangesSet = exchanges.map(pairs =>
      Set([pairs.get('source').get('name'),
           pairs.get('target').get('name')])).toSet();
    return previousData.takeLast(uniqueYears).map(year =>
      year.map(pairs =>
        Set([pairs.get('source'),
             pairs.get('target')])
          .toSet())).reduce((redundancies, yearSet) =>
            redundancies + Set.intersect([
              yearSet,
              exchangesSet,
            ]).size, 0) <= repeatsAllowed;
  }
  return diverse() && notRedundant();
}

function exchangeFormat(pairs, year) {
  return pairs.map(pair => Map({
    source: pair.get('source').get('name'),
    target: pair.get('target').get('name'),
    value: year,
  }));
}

export function generateValidPairings(
  fam, year,
  previousData,
) {
  const shuffled = fam.sortBy(Math.random);
  const pairings = giftFam(shuffled, year);
  if (validatePairings(pairings, previousData)) {
    return exchangeFormat(pairings, year);
  }
  return generateValidPairings(fam, year, previousData);
}

function compileData() {
  function genNew(currentData, mData) {
    if (mData.isEmpty()) {
      return currentData;
    }
    const yearInfo = mData.first();
    return genNew(
      currentData.set(
        mData.keyOf(yearInfo),
        generateValidPairings(
          yearInfo.get('members'),
          parseInt(mData.keyOf(yearInfo)),
          currentData,
        ),
      ),
      mData.rest(),
    );
  }
  return genNew(legacyData, membersData);
}

export const exchangeHistoryGen = compileData();
console.log(JSON.stringify(exchangeHistoryGen,null,1));
