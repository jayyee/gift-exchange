import { Map, List, OrderedMap, fromJS } from 'immutable';
import famMembers from './members.json';

const members = fromJS(famMembers);

function remove(participants, names) {
  return participants.filter(member =>
    names.reduce((matches, name) =>
      matches && (!member.get('name').match(name)), true));
}

// Example usage of remove:
// const members2020 = remove(members, List(['333', 'Christopher', 'DJPUPPY', 'JULIE']));

// 2020: Map({ seed: 2020, members: members2020 }),

export const membersData = OrderedMap({
  2018: Map({ members }),
  // 2019: Map({ members }),
});
