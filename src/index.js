import { Map, List, fromJS, OrderedMap } from 'immutable';
import exchangeHistoryJSON from './exchangeHistory.json';
// import { exchangeHistoryGen } from './pair';
import { graphIt } from './graphIt';

const exchangeHistory = fromJS(exchangeHistoryJSON).toOrderedMap();
// const exchangeHistory = exchangeHistoryGen;

function poolNodes(activeYears) {
  const allExchanges = activeYears.map(yearExchanges =>
    yearExchanges.map(exchanges =>
      exchanges.get('source'))).toSet().flatten();
  return allExchanges.toList().reduce((acc, name) =>
    acc.push(Map({ id: name })), List([]));
}

function toD3(activeExchanges) {
  return Map({
    nodes: poolNodes(activeExchanges),
    links: activeExchanges.toList().flatten(1),
  });
}

function calcActive() {
  const activeButtons = document
    .getElementsByClassName('button is-active');
  function addActive(activeHistory, counter) {
    if (counter >= activeButtons.length) {
      return activeHistory;
    }
    return addActive(activeHistory.set(
      activeButtons[counter].textContent,
      exchangeHistory.get(activeButtons[counter].textContent),
    ), counter + 1);
  }
  return addActive(new Map(), 0);
}

function destroyAndGraph() {
  const graph = document.getElementById('graph');
  const svg = document.getElementsByTagName('svg')[0];
  if (svg !== undefined) {
    graph.removeChild(svg);
  }
  const currentD3Data = toD3(calcActive());
  graphIt(currentD3Data.toJS());
}

function toggleActive(element) {
  if (element.getAttribute('class').match('is-active')) {
    element.setAttribute('class', 'button is-success is-outlined');
  } else {
    element.setAttribute('class', 'button is-active is-success');
  }
  destroyAndGraph();
}

function init() {
  const buttons = document.getElementsByClassName('buttons')[0];
  exchangeHistory.forEach((exchanges, year) => {
    const yearButton = document.createElement('a');
    if (year === exchangeHistory.keySeq().last()) {
      yearButton.setAttribute('class', 'button is-active is-success');
    } else {
      yearButton.setAttribute('class', 'button is-success is-outlined');
    }
    yearButton.addEventListener('click', () =>
      toggleActive(yearButton));
    const yearText = document.createTextNode(year.toString());
    yearButton.appendChild(yearText);
    buttons.appendChild(yearButton);
  });
  destroyAndGraph();
}

init();
