import * as d3 from 'd3';

export function graphIt(graph) {
  const width = 960;
  const height = 600;

  const svg = d3
    .select('#graph')
    .append('svg')
    .attr('width', width)
    .attr('height', height);

  const color = d3.scaleOrdinal(d3.schemeCategory10);

  const simulation = d3
    .forceSimulation()
    .force('link', d3.forceLink()
                     .id(d => d.id)
                     .distance(70))
    .force('charge', d3.forceManyBody()
                       .strength(-65)
                       .distanceMax(300))
    .force('center', d3.forceCenter(width / 2, height / 2));

  const nodes = graph.nodes;
  const nodeById = d3.map(nodes, d => d.id);
  const links = graph.links;
  const bilinks = [];

  links.forEach((link) => {
    link.source = nodeById.get(link.source);
    const s = link.source;
    link.target = nodeById.get(link.target);
    const t = link.target;
    const i = {}; // intermediate node
    const c = link.value;
    nodes.push(i);
    links.push(
      { source: s, target: i },
      { source: i, target: t },
    );
    bilinks.push([s, i, t, c]);
  });

  const node = svg.selectAll('.node')
                  .data(nodes.filter(d => d.id))
                  .enter().append('circle')
                  .attr('class', 'node')
                  .attr('r', 5)
                  .attr('fill', d => color(d.id))
                  .call(d3.drag()
                          .on('start', dragstarted)
                          .on('drag', dragged)
                          .on('end', dragended));

  const text = svg.append('g')
                  .attr('class', 'names')
                  .selectAll('text')
                  .data(nodes)
                  .enter().append('text')
                  .attr('x', '0')
                  .attr('y', '0')
                  .text(d => d.id);

  const markers = svg.append('defs').selectAll('marker')
                     .data(['anything'])
                     .enter()
                     .append('marker')
                     .attr('id', d => d)
                     .attr('viewBox', '0 -5 10 10')
                     .attr('refX', 15)
                     .attr('refY', -1.5)
                     .attr('markerWidth', 10)
                     .attr('markerHeight', 10)
                     .attr('orient', 'auto')
                     .append('path')
                     .attr('fill', '#98df8a')
                     .attr('d', 'M0,-5L10,0L0,5');

  const link = svg.selectAll('.link')
                  .data(bilinks)
                  .enter().append('path')
                  .attr('class', 'link')
                  .attr('stroke-width', 1)
                  .attr('stroke', d => color(d[3]))
                  .attr('class', () => 'link anything')
                  .attr('marker-end', () => 'url(#anything)');

  node.append('title')
      .text(d => d.id);

  simulation
    .nodes(nodes)
    .on('tick', ticked);

  simulation.force('link')
            .links(links);

  function ticked() {
    link.attr('d', positionLink);
    node.attr('transform', positionNode);
    text.attr('x', d => d.x)
        .attr('y', d => d.y);
    markers.attr('x', d => d.x)
           .attr('y', d => d.y);
  }

  function positionLink(d) {
    return 'M' + d[0].x + ',' + d[0].y
         + 'S' + d[1].x + ',' + d[1].y
         + ' ' + d[2].x + ',' + d[2].y;
  }

  function positionNode(d) {
    return 'translate(' + d.x + ',' + d.y + ')';
  }

  function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.9).restart();
    d.fx = d.x;
    d.fy = d.y;
  }

  function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0.1);
    d.fx = null;
    d.fy = null;
  }
}
